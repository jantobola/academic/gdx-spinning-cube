package com.jantobola.spinningcube.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.utils.Pool;
import com.jantobola.spinningcube.physics.SimpleMotionState;

/**
 * PhysicsComponent
 *
 * @author Jan Tobola, 2015
 */
public class PhysicsComponent extends Component implements Pool.Poolable {

    public float mass;
    public Vector3 localInertia = new Vector3(0, 0, 0);

    public btRigidBody body;
    public btCollisionShape collisionShape;

    public btRigidBody.btRigidBodyConstructionInfo constructionInfo;
    public SimpleMotionState motionState;

    @Override
    public void reset() {
        mass = 0;
        localInertia.set(0, 0, 0);

        motionState.dispose();
        motionState = null;

        constructionInfo.dispose();
        collisionShape.dispose();
        body.dispose();
    }

}
