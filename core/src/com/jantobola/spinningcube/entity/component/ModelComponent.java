package com.jantobola.spinningcube.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.utils.Pool;

/**
 * ModelComponent
 *
 * @author Jan Tobola, 2015
 */
public class ModelComponent extends Component implements Pool.Poolable {

    public ModelInstance modelInstance;

    public ModelComponent() {
    }

    public ModelComponent(ModelInstance modelInstance) {
        this.modelInstance = modelInstance;
    }

    @Override
    public void reset() {
        modelInstance = null;
    }

}
