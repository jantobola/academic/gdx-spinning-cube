package com.jantobola.spinningcube.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

/**
 * CharacterComponent
 *
 * @author Jan Tobola, 2015
 */
public class CharacterComponent extends Component implements Pool.Poolable {

    @Override
    public void reset() {

    }

}
