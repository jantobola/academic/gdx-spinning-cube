package com.jantobola.spinningcube.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.jantobola.common.entity.EntityEngine;
import com.jantobola.common.entity.component.mapper.Mapper;
import com.jantobola.common.entity.system.EntitySystem;
import com.jantobola.spinningcube.entity.component.ModelComponent;

/**
 * ModelRenderingSystem
 *
 * @author Jan Tobola, 2015
 */
public class ModelRenderingSystem extends EntitySystem {

    @Mapper
    private ComponentMapper<ModelComponent> modelComponentMapper;
    private Camera camera;
    private Environment environment;

    private ModelBatch batch;

    public ModelRenderingSystem(Camera camera, Environment environment) {
        super();
        this.camera = camera;
        this.environment = environment;
    }

    public ModelRenderingSystem(int priority, Camera camera, Environment environment) {
        super(priority);
        this.camera = camera;
        this.environment = environment;
    }

    @Override
    protected Family filter() {
        return Family.all(ModelComponent.class).get();
    }

    @Override
    protected void addedCallback(EntityEngine engine) {
        batch = new ModelBatch();
    }

    @Override
    public void update(float deltaTime) {

        batch.begin(camera);

        for(int i = 0; i < entities.size(); i++) {
            ModelComponent modelComponent = modelComponentMapper.get(entities.get(i));
            batch.render(modelComponent.modelInstance, environment);
        }

        batch.end();
    }

    @Override
    protected void removedCallback(EntityEngine engine) {
        batch.dispose();
    }

}
