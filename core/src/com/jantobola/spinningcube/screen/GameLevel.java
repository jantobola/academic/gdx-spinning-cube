package com.jantobola.spinningcube.screen;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.physics.bullet.dynamics.*;
import com.badlogic.gdx.utils.TimeUtils;
import com.jantobola.common.entity.EntityEngine;
import com.jantobola.common.entity.component.mapper.Mapper;
import com.jantobola.common.entity.system.EntityComponentSystem;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.loadable.Screen3D;
import com.jantobola.spinningcube.entity.component.CharacterComponent;
import com.jantobola.spinningcube.entity.component.ModelComponent;
import com.jantobola.spinningcube.entity.component.MovingComponent;
import com.jantobola.spinningcube.entity.component.PhysicsComponent;
import com.jantobola.spinningcube.entity.system.CharacterControllerSystem;
import com.jantobola.spinningcube.entity.system.ModelRenderingSystem;
import com.jantobola.spinningcube.physics.SimpleMotionState;
import com.jantobola.spinningcube.physics.WinningContactListener;

/**
 * GameLevel
 *
 * @author Jan Tobola, 2015
 */
public class GameLevel extends Screen3D implements EntityComponentSystem {

	// Heads up display
	HUDScreen hud;

	// Model loaded from disk
	Model model;
	Music music;

	// Only camera in scene
	PerspectiveCamera camera;
	// Controller for the camera
	CameraInputController controller;

	// One environment for now
	Environment environment;

	// Physics
	btCollisionConfiguration collisionConfig;
	btDispatcher dispatcher;
	btBroadphaseInterface broadphase;
	btDynamicsWorld dynamicsWorld;
	btConstraintSolver constraintSolver;
	WinningContactListener winListener;

	PhysicsComponent characterPhysics;

	long startTime;
	long currentTime;

	boolean levelEnd = false;
	boolean timeSet = false;

	// Physics flags
	private final static short FINISH_BLOCK = 1<<8;
	private final static short CHARACTER = 1<<9;
	private final static short BLOCK = -1;

	private CharacterControllerSystem characterController;
	private EntityEngine entityEngine;

	public GameLevel(ScreenManager screenManager) {
		super(screenManager);
	}

	@Override
	protected void requestAssets() {

		// since the Screen3D is a LoadableScreen, we are able to ask asset manager directly with loadable screen's
		// methods to add these requests to assets queue
		requestAsset("cube", "models/character/rot.g3dj", Model.class);
		requestAsset("music", "sounds/music/level_song.mp3", Music.class);
	}

	private void loadAssets() {

		// Loadable screen has also methods for fetching already loaded assets.
		// In this point it's a real instance managed by asset manager, so there
		// is no need for disposing these assets. Loadable screen does this automatically
		// when hide() method is called. It will unload only these assets that are releasable.
		// Every asset is releasable by default. You can change this by adding last boolean parameter
		// in requestAsset() method with false value. In that case asset manager will hold these assets
		// in memory and next loading will be faster.
		model = getAsset("cube");
		music = getAsset("music");
	}

	@Override
	public void show() {

		// assets are loaded by asset manager in this point, so let's load our assets
		loadAssets();

		// create heads up display for current screen
		hud = new HUDScreen(screenManager, this);
		hud.show();

		// create camera here since we have only one camera in scene, there is no need for
		// creating camera as a single entity or have it as a component.
		camera = new PerspectiveCamera(62, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.position.set(0, 0, 15);
		camera.lookAt(0, 0, 0);
		camera.normalizeUp();
		camera.update();

		// set input controller (this should be a temporary solution)
		controller = new CameraInputController(camera);
		Gdx.input.setInputProcessor(new InputMultiplexer(Gdx.input.getInputProcessor()));

		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.5f, 0.5f, 0.5f, 1));
		environment.add(new DirectionalLight().set(Color.WHITE, -0.4f, -1, -0.3f));

		music.setVolume(0.35f);
		music.setLooping(true);

		levelEnd = false;
		timeSet = false;
		startTime = 0;
		currentTime = 0;
	}

	private void createWorld(EntityEngine entityEngine) {

		// create physical world
		createPhysics();

		for(int i = 0; i < 60; i = i + 2) {
			if(i == 48 || i == 50 || i == 52) continue;

			// ask entity engine to create entity from its pool
			Entity entity = entityEngine.createEntity();

			// just make the model instance from common model
			ModelInstance modelInstance = new ModelInstance(model);

			if(i == 0) {
				modelInstance.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Ambient, Color.BLUE));
				modelInstance.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Diffuse, Color.BLUE));
			}

			if(i == 58) {
				modelInstance.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Ambient, Color.RED));
				modelInstance.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Diffuse, Color.RED));
			}

			// create components
			// create model component
			ModelComponent modelComponent = entityEngine.createComponent(ModelComponent.class);
			modelComponent.modelInstance = modelInstance;


			// create physics component
			PhysicsComponent physicsComponent = entityEngine.createComponent(PhysicsComponent.class);

			physicsComponent.collisionShape = new btBoxShape(new Vector3(1, 1, 1));
			if (physicsComponent.mass > 0) {
				physicsComponent.collisionShape.calculateLocalInertia(physicsComponent.mass, physicsComponent.localInertia);
			}
			physicsComponent.constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(physicsComponent.mass, null, physicsComponent.collisionShape, physicsComponent.localInertia);
			physicsComponent.body = new btRigidBody(physicsComponent.constructionInfo);
			physicsComponent.motionState = new SimpleMotionState();
			physicsComponent.motionState.transform = modelComponent.modelInstance.transform;
			physicsComponent.body.setMotionState(physicsComponent.motionState);
			physicsComponent.body.setCollisionFlags(physicsComponent.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_STATIC_OBJECT);

			// create simple map
			if(i < 20)
				modelComponent.modelInstance.transform.translate(i, 0, 0);
			if(i < 40 && i >= 20)
				modelComponent.modelInstance.transform.translate(20, 0, -i + 20);
			if(i < 60 && i >= 40)
				modelComponent.modelInstance.transform.translate(i - 20, 0, -20);

			// set references to model matrix to keep synchronized states
			physicsComponent.body.proceedToTransform(modelComponent.modelInstance.transform);

			dynamicsWorld.addRigidBody(physicsComponent.body);

			if(i == 58) {
				physicsComponent.body.setContactCallbackFlag(FINISH_BLOCK);
				physicsComponent.body.setContactCallbackFilter(0);
			}

			// add components to entity
			entity.add(modelComponent);
			entity.add(physicsComponent);
			// add the entity to the entity engine
			entityEngine.addEntity(entity);
		}

		createObstacle(6, 2, 0, entityEngine);

		createObstacle(17, 2, -10, entityEngine);
		createObstacle(23, 2, -10, entityEngine);
		createObstacle(20, 4, -10, entityEngine);

		createObstacle(20, 2, -20, entityEngine);

		createObstacle(28, -2, -20, entityEngine);
		createObstacle(30, -2, -20, entityEngine);
		createObstacle(32, -2, -20, entityEngine);

		createObstacle(30, 4, -20, entityEngine);
		createObstacle(30, 6, -20, entityEngine);
		createObstacle(30, 8, -20, entityEngine);

	}

	private void createObstacle(int x, int y, int z, EntityEngine entityEngine) {
		// ask entity engine to create entity from its pool
		Entity entity = entityEngine.createEntity();

		// just make the model instance from common model
		ModelInstance modelInstance = new ModelInstance(model);

		// create components
		// create model component
		ModelComponent modelComponent = entityEngine.createComponent(ModelComponent.class);
		modelComponent.modelInstance = modelInstance;

		modelInstance.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Ambient, Color.BLACK));
		modelInstance.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Diffuse, Color.BLACK));

		modelInstance.getMaterial("Material").set(new BlendingAttribute(true, 0.5f));

		// create physics component
		PhysicsComponent physicsComponent = entityEngine.createComponent(PhysicsComponent.class);

		physicsComponent.collisionShape = new btBoxShape(new Vector3(1, 1, 1));
		if (physicsComponent.mass > 0) {
			physicsComponent.collisionShape.calculateLocalInertia(physicsComponent.mass, physicsComponent.localInertia);
		}
		physicsComponent.constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(physicsComponent.mass, null, physicsComponent.collisionShape, physicsComponent.localInertia);
		physicsComponent.body = new btRigidBody(physicsComponent.constructionInfo);
		physicsComponent.motionState = new SimpleMotionState();
		physicsComponent.motionState.transform = modelComponent.modelInstance.transform;
		physicsComponent.body.setMotionState(physicsComponent.motionState);
		physicsComponent.body.setCollisionFlags(physicsComponent.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_STATIC_OBJECT);


		modelComponent.modelInstance.transform.translate(x, y, z);

		// set references to model matrix to keep synchronized states
		physicsComponent.body.proceedToTransform(modelComponent.modelInstance.transform);

		dynamicsWorld.addRigidBody(physicsComponent.body);

		// add components to entity
		entity.add(modelComponent);
		entity.add(physicsComponent);
		// add the entity to the entity engine
		entityEngine.addEntity(entity);
	}

	private void createPhysics() {
		collisionConfig = new btDefaultCollisionConfiguration();
		dispatcher = new btCollisionDispatcher(collisionConfig);
		broadphase = new btDbvtBroadphase();
		constraintSolver = new btSequentialImpulseConstraintSolver();
		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, constraintSolver, collisionConfig);
		dynamicsWorld.setGravity(new Vector3(0, -10f, 0));
	}

	private void createCharacter(EntityEngine entityEngine) {

		// create character entity from entity pool
		Entity entity = entityEngine.createEntity();

		// create temp character
		ModelInstance character = new ModelInstance(model);
		// make it look different to other blocks
		character.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Ambient, Color.GREEN));
		character.getMaterial("Material").set(new ColorAttribute(ColorAttribute.Diffuse, Color.GREEN));

		// create model component
		ModelComponent modelComponent = entityEngine.createComponent(ModelComponent.class);
		modelComponent.modelInstance = character;

		// create character component - add new component to tag this entity as a character
		CharacterComponent characterComponent = entityEngine.createComponent(CharacterComponent.class);

		// create physics component
		PhysicsComponent physicsComponent = entityEngine.createComponent(PhysicsComponent.class);

		characterPhysics = physicsComponent;

		physicsComponent.mass = 2;
		physicsComponent.collisionShape = new btBoxShape(new Vector3(1, 1, 1));
		if (physicsComponent.mass > 0) {
			physicsComponent.collisionShape.calculateLocalInertia(physicsComponent.mass, physicsComponent.localInertia);
		}
		physicsComponent.constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(physicsComponent.mass, null, physicsComponent.collisionShape, physicsComponent.localInertia);
		physicsComponent.body = new btRigidBody(physicsComponent.constructionInfo);
		physicsComponent.motionState = new SimpleMotionState();
		physicsComponent.motionState.transform = modelComponent.modelInstance.transform;
		physicsComponent.body.setMotionState(physicsComponent.motionState);
		physicsComponent.body.setCollisionFlags(physicsComponent.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
		physicsComponent.body.setActivationState(CollisionConstants.DISABLE_DEACTIVATION);
		physicsComponent.body.setFriction(1);

		physicsComponent.body.setContactCallbackFlag(CHARACTER);
		physicsComponent.body.setContactCallbackFilter(FINISH_BLOCK);

		modelComponent.modelInstance.transform.translate(0.2f, 3, 0);

		// set references to model matrix to keep synchronized states
		physicsComponent.body.proceedToTransform(modelComponent.modelInstance.transform);

		MovingComponent movingComponent = entityEngine.createComponent(MovingComponent.class);

		dynamicsWorld.addRigidBody(physicsComponent.body);

		// add components
		entity.add(modelComponent);
		entity.add(characterComponent);
		entity.add(physicsComponent);
		entity.add(movingComponent);

		// add entity
		entityEngine.addEntity(entity);
	}

	@Override
	public void create(EntityEngine entityEngine) {

		this.entityEngine = entityEngine;

		// initialize bullet physics
		Bullet.init();

		// create our map
		createWorld(entityEngine);
		winListener = new WinningContactListener(this);

		// create our character
		createCharacter(entityEngine);

		// add rendering system that can render all models in scene
		entityEngine.addSystem(new ModelRenderingSystem(camera, environment));
		this.characterController = new CharacterControllerSystem(camera);
		entityEngine.addSystem(this.characterController);

		music.play();
	}

	@Override
	public void render(float delta) {

		if(startTime == 0) {
			startTime = TimeUtils.millis();
		}

		camera.update();

		if(!levelEnd) {
			// update physics
			final float deltaStep = Math.min(1f / 30f, delta);
			dynamicsWorld.stepSimulation(deltaStep, 5, 1.0f / 60.0f);

			currentTime = TimeUtils.millis();
			hud.timeLabel.setText(String.valueOf((currentTime - startTime) / 1000.0f));
		}

		if(levelEnd && !timeSet) {
			hud.winLabel.setText(hud.winLabel.getText() + "\n" + String.valueOf((currentTime - startTime) / 1000.0f));
			timeSet = true;
		}

		// just render our HUD, other rendering is done by our rendering systems
		hud.render(delta);
	}

	@Override
	public void hide() {
		// dispose HUD here since we're creating new instance every time the screen is shown
		hud.dispose();

		dynamicsWorld.dispose();
		constraintSolver.dispose();
		broadphase.dispose();
		dispatcher.dispose();
		collisionConfig.dispose();
		winListener.dispose();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		// don't forget to call HUD resize
		hud.resize(width, height);
	}

	@Mapper
	private ComponentMapper<PhysicsComponent> pc;

	public void roundWin() {
		hud.showWinningBox();
		levelEnd = true;
	}

	public void rotateCamera() {
		characterController.rotateCamera();
	}

}
