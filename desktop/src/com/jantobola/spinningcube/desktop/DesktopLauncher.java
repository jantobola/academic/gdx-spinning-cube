package com.jantobola.spinningcube.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jantobola.spinningcube.Application;

public class DesktopLauncher {

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.useGL30 = false;
		config.width = 1024;
		config.height = 768;
		config.foregroundFPS = 0;
		config.samples = 8;
		config.vSyncEnabled = true;

		new LwjglApplication(new Application(), config);
	}

}
